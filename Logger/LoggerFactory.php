<?php

class LoggerFactory
{
    public static function getInstance($class,  array $options = [])
    {
        if(!class_exists($class) ) {
                throw new Exception('|Could not load class or name is incorrect! |');
        }

        return new $class($options);

    }



}