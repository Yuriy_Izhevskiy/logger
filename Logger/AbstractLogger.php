<?php

abstract class AbstractLogger implements LoggerInterface
{
    /**
     * Critical conditions.
     *
     * Example: Application component unavailable, unexpected exception.
     * @param $message
     * @param array $context
     * @return $this
     */
    public function critical($message, array $context = [])
    {
        $this->log(LoggerInterface::CRITICAL, $message, $context);
        return $this;
    }

    /**
     * Runtime errors that do not require immediate action but should typically
     * be logged and monitored.
     * @param string $message
     * @param array $context
     * @return $this|null
     */
    public function error($message, array $context = [])
    {
        $this->log(LoggerInterface::ERROR, $message, $context);
        return $this;
    }

    /**
     * Exceptional occurrences that are not errors.
     *
     * Example: Use of deprecated APIs, poor use of an API, undesirable things
     * that are not necessarily wrong.
     * @param string $message
     * @param array $context
     * @return $this|null
     */
    public function warning($message, array $context = [])
    {
        $this->log(LoggerInterface::WARNING, $message, $context);
        return $this;
    }

    /**
     * Normal but significant events.
     *
     * @param string $message
     * @param array $context
     * @return $this|null
     */
    public function notice($message, array $context = [])
    {
        $this->log(LoggerInterface::NOTICE, $message, $context);
        return $this;
    }

    /**
     * Interesting events.
     *
     * Example: User logs in, SQL logs.
     * @param string $message
     * @param array $context
     * @return $this|null
     */
    public function info($message, array $context = [])
    {
        $this->log(LoggerInterface::INFO, $message, $context);
        return $this;
    }

    /**
     * Detailed debug information.
     * @param string $message
     * @param array $context
     * @return $this|null
     */
    public function debug($message, array $context = [])
    {
        $this->log(LoggerInterface::DEBUG, $message, $context);
        return $this;
    }
}