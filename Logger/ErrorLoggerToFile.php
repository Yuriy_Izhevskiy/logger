<?php
loadConfig('utils');

/**
 * Class ErrorLoggerToFile
 * Example:
 * 1. $logger = LoggerFactory::getInstance('ErrorLoggerToFile',
                [
                    'logDirectory' => '/var/www/control/app/logs',
                    'filename'=> 'api_logger',
                    'extension' => 'log',
                    'logFormat' => '{date} - {level} - {message}'
                ]);
 * 2. $logger = LoggerFactory::getInstance('ErrorLoggerToFile');
 *    $logger->debug('TEST' . $ex, $users);
 */
class ErrorLoggerToFile extends AbstractLogger
{
    private $defaultPermissions = 0777;
    private $logFilePath;
    private $fileHandle;
    private $lastLine = '';
    private $level;

    const PATH_ERROR_LOG = '/var/www/control/app/logs/error_logs.log';

    const DEF_LOG_DIRECTORY_PATH = '/var/www/control/app/logs';
    const DEF_FILE_NAME = 'important_logs';

    protected static  $logLevels = [
        self::CRITICAL  => 500,
        self::ERROR     => 400,
        self::WARNING   => 300,
        self::NOTICE    => 250,
        self::INFO      => 200,
        self::DEBUG     => 100,
      ];

    protected $options = [
        'logDirectory'   => self::DEF_LOG_DIRECTORY_PATH,
        'dateFormat'     => 'Y-m-d G:i:s.u',
        'extension'      => 'log',
        'filename'       => false,
        'logFormat'      => false,
        'appendContext'  => true,
    ];

    public function __construct(array $options = []) {
        $this->options = array_merge($this->options, $options);
        $this->stream($this->options);
    }

    /**
     * Set the minimum logging level.
     *
     * @throws InvalidArgumentException if the logging level is unknown.
     * @param  integer $level The minimum logging level which will be written
     */
    public function setLevel($level)
    {
        try {
            if (!array_key_exists($level, self::$logLevels)) {
                throw new InvalidArgumentException(sprintf('|Unexpected logging level: %s|', $level));
            }
        } catch (\Throwable $ex) {
            error_log( "[{$this->getTimestamp()}]: ". " Fatal Error: " . $ex, 3, self::PATH_ERROR_LOG);
        }

        $this->level = $level;
    }

    public function stream(array $options) : bool
    {
        try {
            $logDirectory = rtrim($options['logDirectory'], DIRECTORY_SEPARATOR);
            if (!file_exists($logDirectory)) {
                $status = mkdir($logDirectory, $this->defaultPermissions, true);

                if (false === $status && !is_dir($logDirectory)) {
                    throw new \Exception("|Directory is missing and not created|");
                }
            }
            if ($logDirectory) {
                $this->setLogFilePath($logDirectory);
            }
            if (file_exists($this->logFilePath) && !is_writable($this->logFilePath)) {
                throw new RuntimeException('|The file could not be written to. Check that appropriate permissions have been set.|');
            }
            $this->fileHandle = fopen($this->logFilePath, 'a');

            if ( !$this->fileHandle || !is_resource($this->fileHandle)) {
                throw new RuntimeException('|The file could not be opened. Check permissions.|');
            }
        } catch (\Throwable $ex) {
            error_log( "[{$this->getTimestamp()}]: ". " Fatal Error: " . $ex, 3, self::PATH_ERROR_LOG);
            return false;
        }
        return true;
    }

    public function __destruct()
    {
        if ($this->fileHandle) {
            fclose($this->fileHandle);
        }
    }

    /**
     * @param $logDirectory
     */
    public function setLogFilePath($logDirectory)
    {
        if ($this->options['filename']) {
            if (strpos($this->options['filename'], '.log') !== false || strpos($this->options['filename'], '.txt') !== false) {
                $this->logFilePath = $logDirectory .DIRECTORY_SEPARATOR. $this->options['filename'];
            } else {
                $this->logFilePath = $logDirectory .DIRECTORY_SEPARATOR. $this->options['filename'] .'.'. $this->options['extension'];
            }
        } else {
            $this->logFilePath = $logDirectory .DIRECTORY_SEPARATOR. self::DEF_FILE_NAME .'.'. $this->options['extension'];
        }
    }

    /**
     * Writes a line to the log without prepending a status or timestamp
     * @param $level
     * @param $message
     * @param array $context
     * @throws Throwable
     */
    public function log($level, $message, array $context = [])
    {
        $this->setLevel($level);
        $message = $this->formatMessageForFile($level, $message, $context);

        if (self::$logLevels[$level] >= self::$logLevels[$this->level]) {
            $this->writeLog($message);
        }
    }

    /**
     * Get the file path that the log is currently writing to
     *
     * @return string
     */
    public function getLogFilePath() : string
    {
        return $this->logFilePath;
    }

    protected function getLevelName($level) : string
    {
        return strtoupper($level);
    }

    public function writeLog($message)
    {
        if (null !== $this->fileHandle) {
            if (fwrite($this->fileHandle, $message) === false) {
                throw new RuntimeException('|The file could not be written to. Check that appropriate permissions have been set.|');
            } else {
                $this->lastLine = trim($message);
            }
        }
    }

    /**
     * @param $level
     * @param string $message
     * @param array $context
     * @return string
     */
    protected function formatMessageForFile($level,  $message, array $context)
    {
        try {
            if ($this->options['logFormat']) {
                $parts = [
                    'date'          => $this->getTimestamp(),
                    'level'         => $this->getLevelName($level),
                    'priority'      => self::$logLevels[$level],
                    'level-padding' => str_repeat(' ', 9 - strlen($level)),
                    'message'       => $message,
                    'context'       => json_encode($context),
                ];

                $message = $this->options['logFormat'];

                foreach ($parts as $part => $value) {
                    $message = str_replace('{'.$part.'}', $value, $message);
                }
            } else {
                $message = "[{$this->getTimestamp()}] :: [{$this->getLevelName($level)}] :: {$message}";
            }

            if ($this->options['appendContext'] && ! empty($context)) {
                $message .= PHP_EOL.$this->indent($this->contextToString($context));
            }

        } catch (\Throwable $ex) {
            error_log( "[{$this->getTimestamp()}]: ". " Fatal Error: " . $ex, 3, self::PATH_ERROR_LOG);
        }

        return $message . PHP_EOL;
    }

    private function getTimestamp()
    {
        $originalTime = microtime(true);
        $micro = sprintf("%06d", ($originalTime - floor($originalTime)) * 1000000);
        $date = new DateTime(date('Y-m-d H:i:s.'.$micro, $originalTime));
        return $date->format($this->options['dateFormat']);
    }

    protected function contextToString($context)
    {
        $export = '';
        foreach ($context as $key => $value) {
            $export .= "{$key}: ";

            $export .= preg_replace(array(
                '/=>\s+([a-zA-Z])/im',
                '/array\(\s+\)/im',
                '/^  |\G  /m'
            ), array(
                '=> $1',
                'array()',
                '    '
            ), str_replace('array (', 'array(', var_export($value, true)));
        }
        $export .= PHP_EOL;
        return str_replace(array('\\\\', '\\\''), array('\\', '\''), rtrim($export));
    }

    protected function indent($string, $indent = '    ')
    {
        return $indent.str_replace("\n", "\n".$indent, $string);

    }

}