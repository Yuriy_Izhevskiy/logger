# Logger
Класс ErrorLoggerToFile позволят логировать данные в файл и розширяет LoggerInterface.
Принимает ряд настроек (options) для удобства.

### Example:

```

 1. $logger = LoggerFactory::getInstance('ErrorLoggerToFile',
                [
                    'logDirectory' => '/var/www/control/app/logs',
                    'filename'=> 'api_logger',
                    'extension' => 'log',
                    'logFormat' => '{date} - {level} - {message}'
                ]);



 2. $logger = LoggerFactory::getInstance('ErrorLoggerToFile');
	$logger->debug('TEST' . $ex, $users);
```
 



